<?php

namespace App\Http\Controllers;

use App\Classes\Dto\ProductDto;
use App\Http\Requests\FilterRequest;
use App\Http\Requests\OffsetRequest;
use App\Http\Requests\Product\ProductIndexRequest;
use App\Http\Requests\Product\ProductStoreRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Classes\Contracts\Services\ProductService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{
    /**
     * @var ProductService $productService
     */
    private ProductService $productService;

    /**
     * ProductController constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param OffsetRequest $offsetRequest
     * @param FilterRequest $filterRequest
     * @return AnonymousResourceCollection
     */
    public function index(OffsetRequest $offsetRequest, FilterRequest $filterRequest): AnonymousResourceCollection
    {
        $offsetDto = $offsetRequest->getDto();

        $products = $this->productService->getProductList(
            $offsetDto, $filterRequest->getDto()
        );

        return ProductResource::collection($products->items)
            ->additional([
                'status' => 'success',
                'meta' => [
                    'paginate' => $products->meta($offsetDto->offset, $offsetDto->limit),
                ]
            ]);
    }

    /**
     * @param ProductStoreRequest $request
     * @return ProductResource
     */
    public function store(ProductStoreRequest $request): ProductResource
    {
        $product = $this->productService->saveProduct(
            new ProductDto([
                'title' => $request->title,
                'description' => $request->description,
                'price' => (float)$request->price,
                'is_published' => (bool)$request->is_published,
                'categories' => $request->categories,
            ])
        );

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param ProductUpdateRequest $request
     * @param int $id
     * @return ProductResource
     */
    public function update(ProductUpdateRequest $request, int $id): ProductResource
    {
        $product = Product::findOrFail($id);

        $product = $this->productService->updateProduct(
            new ProductDto([
                'title' => $request->title,
                'description' => $request->description,
                'price' => (float)$request->price,
                'is_published' => (bool)$request->is_published,
                'categories' => $request->categories,
            ]),
            $product
        );

        return new ProductResource($product);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $product = Product::findOrFail($id);
        $this->productService->deleteProduct($product);

        return response()->json([
            'status' => 'success',
        ]);
    }
}
