<?php

namespace App\Http\Controllers;

use App\Classes\Dto\CategoryDto;
use App\Classes\Exceptions\CategoryHasProductsException;
use App\Http\Requests\Category\CategoryUpdateRequest;
use App\Classes\Contracts\Services\CategoryService;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\Category\CategoryStoreRequest;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * @var CategoryService $categoryService
     */
    private CategoryService $categoryService;

    /**
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return CategoryCollection
     */
    public function index(): CategoryCollection
    {
        $categories = $this->categoryService->getCategoryList();
        return new CategoryCollection(
            $categories
        );
    }

    /**
     * @param CategoryStoreRequest $request
     * @return CategoryResource
     */
    public function store(CategoryStoreRequest $request): CategoryResource
    {
        $category = $this->categoryService->saveCategory(
            new CategoryDto([
                'title' => $request->title
            ]));

        return new CategoryResource($category);
    }

    /**
     * @param int $id
     * @return CategoryResource
     */
    public function show(int $id): CategoryResource
    {
        $category = Category::findOrFail($id);

        $category = $this->categoryService->getCategory($category);

        return new CategoryResource($category);
    }

    /**
     * @param CategoryUpdateRequest $request
     * @param int $id
     * @return CategoryResource
     */
    public function update(CategoryUpdateRequest $request, int $id): CategoryResource
    {
        $category = Category::findOrFail($id);

        $category = $this->categoryService->updateCategory(
            new CategoryDto(['title' => $request->title]),
            $category
        );

        return new CategoryResource($category);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $category = Category::findOrFail($id);

        if ($category->products->isNotEmpty()) {
            throw new CategoryHasProductsException();
        }

        $this->categoryService->deleteCategory($category);

        return response()->json([
            'status' => 'success',
        ], Response::HTTP_OK);
    }
}
