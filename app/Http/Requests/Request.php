<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->get('_debug')) {
            dd($validator->errors());
        }

        $responseBody = ['status' => 'failure'];
        /** @var \Illuminate\Validation\Validator $validator */

        throw new HttpResponseException(response()->json($responseBody, Response::HTTP_UNPROCESSABLE_ENTITY));
    }

}
