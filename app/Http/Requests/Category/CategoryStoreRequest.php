<?php

namespace App\Http\Requests\Category;


use App\Http\Requests\Request;

class CategoryStoreRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:categories|string|max:50',
        ];
    }
}
