<?php namespace App\Http\Requests\Product;

use App\Http\Requests\FilterRequest;

/**
 * Class ProductUpdateRequest
 * @package App\Http\Requests
 */
class ProductUpdateRequest extends FilterRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'categories' => 'required',
            'categories.*' => 'required|exists:App\Models\Category,id',
        ];
    }
}
