<?php
declare(strict_types=1);

namespace App\Traits;

use App\Classes\Dto\OffsetPaginationDTO;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 *
 */
trait OffsetPaginationHelper
{
    /**
     * Helper pagination
     * @param Builder|BelongsToMany $builder
     * @param string|array $columns
     * @return OffsetPaginationDTO
     */
    public function offsetPaginate($builder, $columns = ['*']): OffsetPaginationDTO
    {
        $iTotal = $builder->count($columns);
        $obItems = $builder->get($columns);

        return new OffsetPaginationDTO([
            'total' => $iTotal,
            'items' => $obItems,
        ]);
    }
}
