<?php
namespace App\Classes\Exceptions;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;
use Throwable;

/**
 * Class JsonException
 * @package App\Exceptions
 */
class JsonException extends HttpException
{
    /**
     * @inerhitDoc
     */
    protected ?int $statusCode = Response::HTTP_BAD_REQUEST;

    /**
     * @inerhitDoc
     */
    protected $message = 'exceptions.bad_request';


    /**
     * @param int|null       $statusCode
     * @param string|null    $message
     * @param Throwable|null $previous
     * @param array          $headers
     * @param int|null       $code
     */
    public function __construct(
        ?int $statusCode = null,
        ?string $message = null,
        Throwable $previous = null,
        array $headers = [],
        ?int $code = 0
    ) {
        if ($statusCode) {
            $this->statusCode = $statusCode;
        }

        if ($message) {
            $this->message = $message;
        }

        parent::__construct($this->statusCode, $this->message, $previous, $headers, $code);
    }

    /**
     * @param $request
     *
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        return response()->json([
            'status' => 'failed',
            'error'  => [
                'message' => $this->message,
            ],
        ], $this->getStatusCode(), $this->getHeaders());
    }
}
