<?php

namespace App\Classes\Contracts\Services;

use App\Classes\Dto\CategoryDto;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Category;

interface CategoryService
{

    /**
     * @return Collection|null
     */
    public function getCategoryList(): ?Collection;

    /**
     * @param CategoryDto $categoryDto
     * @return Category
     */
    public function saveCategory(CategoryDto $categoryDto): Category;

    /**
     * @param Category $category
     * @return bool
     */
    public function getCategory(Category $category): bool;

    /**
     * @param CategoryDto $categoryDto
     * @param Category $category
     * @return Category
     */
    public function updateCategory(CategoryDto $categoryDto, Category $category): Category;

    /**
     * @param Category $category
     * @return bool
     */
    public function deleteCategory(Category $category): bool;
}
