<?php

namespace App\Classes\Contracts\Services;

use App\Classes\Dto\FilterDto;
use App\Classes\Dto\OffsetDto;
use App\Classes\Dto\OffsetPaginationDTO;
use App\Classes\Dto\ProductDto;
use App\Models\Product;

/**
 * Interface ProductServiceInterface
 */

interface ProductService
{
    /**
     * @param OffsetDto $offsetDto
     * @param FilterDto $filterDto
     * @return OffsetPaginationDTO|null
     */
    public function getProductList(OffsetDto $offsetDto, FilterDto $filterDto): ?OffsetPaginationDTO;

    /**
     * @param ProductDto $productDto
     * @return Product
     */
    public function saveProduct(ProductDto $productDto): Product;

    /**
     * @param ProductDto $productDto
     * @param Product $product
     * @return Product
     */
    public function updateProduct(ProductDto $productDto, Product $product): Product;

    /**
     * @param Product $product
     * @return bool
     */
    public function deleteProduct(Product $product): bool;

}
