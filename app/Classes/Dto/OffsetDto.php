<?php namespace App\Classes\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class OffsetDto extends DataTransferObject
{
    /**
     * @var int
     */
    public int $offset = 0;

    /**
     * @var int
     */
    public ?int $limit  = 10;
}
