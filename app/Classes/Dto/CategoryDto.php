<?php
declare(strict_types=1);

namespace App\Classes\Dto;

use Spatie\DataTransferObject\DataTransferObject;


class CategoryDto extends DataTransferObject
{
    /**
     * @var string
     */
    public string $title;

}
