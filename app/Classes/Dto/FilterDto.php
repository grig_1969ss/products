<?php namespace App\Classes\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class FilterDto extends DataTransferObject
{

    /**
     * @var array
     */
    public array $filter = [];

    /**
     * @var string|null
     */
    public ?string $search = null;
}
