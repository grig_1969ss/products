<?php
declare(strict_types=1);

namespace App\Classes\Dto;

use Spatie\DataTransferObject\DataTransferObject;


class ProductDto extends DataTransferObject
{
    /**
     * @var string
     */
    public string $title;

    /**
     * @var string
     */
    public string $description;

    /**
     * @var float
     */
    public float $price;

    /**
     * @var bool
     */
    public bool $is_published;

    /**
     * @var array
     */
    public array $categories;

}
