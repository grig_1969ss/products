<?php

namespace App\Classes\Dto;

use Illuminate\Contracts\Support\Arrayable;

abstract class DataTransferObject implements Arrayable
{
    /**
     * DataTransferObject constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        $this->unpackParameters($parameters);
    }

    /**
     * @param array $parameters
     */
    protected function unpackParameters(array $parameters): void
    {
        foreach ($parameters as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \RuntimeException(
                    'general.dto.prop_not_exists', [
                    'key' => $key,
                    'class' => static::class,
                ]);
            }

            $this->{$key} = $value;
        }
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $reflection = new \ReflectionClass(static::class);
        $properties = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC);

        $items = [];
        foreach ($properties as $property) {
            if ($property->isStatic()) {
                continue;
            }

            $items[$property->getName()] = $property->getValue($this);
        }

        return $items;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->all();
    }
}
