<?php

namespace App\Services;

use App\Classes\Dto\CategoryDto;
use Illuminate\Database\Eloquent\Collection;
use App\Classes\Contracts\Services\CategoryService as CategoryServiceContract;
use App\Models\Category;

class CategoryService implements CategoryServiceContract
{

    /**
     * @inheritDoc
     */
    public function getCategoryList(): ?Collection
    {
        return Category::all();
    }

    /**
     * @inheritDoc
     */
    public function saveCategory(CategoryDto $categoryDto): Category
    {
        return Category::create(['title' => $categoryDto->title]);
    }

    /**
     * @inheritDoc
     */
    public function getCategory(Category $category): bool
    {
        return $category;
    }

    /**
     * @inheritDoc
     */
    public function updateCategory(CategoryDto $categoryDto, Category $category): Category
    {
        $category->title = $categoryDto->title;
        $category->save();

        return $category;
    }

    /**
     * @inheritDoc
     */
    public function deleteCategory(Category $category): bool
    {
        //todo: so something with products
        return $category->delete();
    }
}
