<?php

namespace App\Services;

use App\Classes\Dto\FilterDto;
use App\Classes\Dto\OffsetDto;
use App\Classes\Dto\OffsetPaginationDTO;
use App\Classes\Dto\ProductDto;
use App\Traits\OffsetPaginationHelper;
use Illuminate\Database\Eloquent\Builder;
use App\Classes\Contracts\Services\ProductService as ProductServiceContract;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductService implements ProductServiceContract
{
    use OffsetPaginationHelper;

    /**
     * @inheritDoc
     */
    public function getProductList(OffsetDto $offsetDto, FilterDto $filterDto): ?OffsetPaginationDTO
    {
        $builder = Product::with('categories')->when(!empty($filterDto->filter), static function (Builder $query) use ($filterDto) {
            $query->when(isset($filterDto->filter['price_from']), function (Builder $query) use ($filterDto) {
                $query->where('price', '>=', $filterDto->filter['price_from']);
            });
            $query->when(isset($filterDto->filter['price_to']), function (Builder $query) use ($filterDto) {
                $query->where('price', '<=', $filterDto->filter['price_to']);
            });
            $query->when(isset($filterDto->filter['is_published']), function (Builder $query) use ($filterDto) {
                $query->where('is_published', (bool)$filterDto->filter['is_published']);
            });
            $query->when(isset($filterDto->filter['categories']), function (Builder $query) use ($filterDto) {
                $query->whereHas('categories', function (Builder $query) use($filterDto) {
                    $query->whereIn('category_id', $filterDto->filter['categories']);
                });

            });
        })->when($filterDto->search, function (Builder $query, string $search) {
            $query->where('title', 'LIKE', "%$search%");
        })
            ->offset($offsetDto->offset)
            ->limit($offsetDto->limit);

        return $this->offsetPaginate($builder);
    }

    /**
     * @inheritDoc
     */
    public function saveProduct(ProductDto $productDto): Product
    {
        return DB::transaction(function () use ($productDto) {
            $product = Product::create([
                'title' => $productDto->title,
                'description' => $productDto->description,
                'price' => $productDto->price,
                'is_published' => $productDto->is_published,
            ]);

            $product->categories()->attach($productDto->categories);

            return $product;
        });

    }

    /**
     * @inheritDoc
     */
    public function updateProduct(ProductDto $productDto, Product $product): Product
    {
        $product->title = $productDto->title;
        $product->description = $productDto->description;
        $product->price = $productDto->price;
        $product->is_published = $productDto->is_published;
        $product->categories()->sync($productDto->categories);
        $product->save();

        return $product;
    }

    /**
     * @inheritDoc
     */
    public function deleteProduct(Product $product): bool
    {
        return $product->delete();
    }

}
